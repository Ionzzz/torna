package cn.torna.sdk.param;

import lombok.Data;

/**
 * @author tanghc
 */
@Data
public class DocParamCode {

    /** code名称 */
    private String code;

    /** 错误描述 */
    private String msg;

    /** 解决方案 */
    private String solution;



}
